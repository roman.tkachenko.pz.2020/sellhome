﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tkachenko.Roman.SellHome.API.Services;
using Tkachenko.Roman.SellHome.Model.Entity;

namespace Tkachenko.Roman.SellHome.API.Controllers {
    [EnableCors("Cors")]
    [Route("api/operation")]
    public class OperationController : Controller {
        private IOperationService service;

        public OperationController(IOperationService service) {
            this.service = service;
        }
        [HttpGet]
        public IActionResult Index() => Ok(service.GetAll());

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id) => Ok(service.Get(id));
        [HttpPost]
        [Route("new")]
        public ActionResult Create([FromBody] Operation entity) {
            service.Add(entity);
            return Ok();
        }

        [HttpPost]
        [Route("update/{id}")]
        public ActionResult Edit(int id, [FromBody] Operation entity) {
            service.Update(id, entity);
            return Ok();
        }

        [HttpGet]
        [Route("delete/{id}")]
        public ActionResult Delete(int id) {
            service.Remove(id);
            return Ok();
        }
    }
}
