﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Tkachenko.Roman.SellHome.API.Services;
using Tkachenko.Roman.SellHome.Model.Entity;

namespace Tkachenko.Roman.SellHome.API.Controllers {
    [EnableCors("Cors")]
    [Route("api/apartment")]
    public class ApartmentController : Controller {
        private IApartmentService service;
        
        public ApartmentController(IApartmentService service) {
            this.service = service;
        }
        [HttpGet]
        public IActionResult Index() => Ok(service.GetAll());
        
        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id) => Ok(service.Get(id));
        [HttpPost]
        [Route("new")]
        public ActionResult Create([FromBody] Apartment entity) {
            service.Add(entity);
            return Ok();
        }

        [HttpPost]
        [Route("update/{id}")]
        public ActionResult Edit(int id, [FromBody] Apartment entity) {
            service.Update(id, entity);
            return Ok();
        }

        [HttpGet]
        [Route("delete/{id}")]
        public ActionResult Delete(int id) {
            service.Remove(id);
            return Ok();
        }
    }
}
