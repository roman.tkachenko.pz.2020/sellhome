﻿using System.Collections.Generic;
using System.Linq;
using Tkachenko.Roman.SellHome.Model;
using Tkachenko.Roman.SellHome.Model.Entity;

namespace Tkachenko.Roman.SellHome.API.Services {
    public class RealtorService : IRealtorService {
        private SHDBContext context;
        public RealtorService() {
            context = new SHDBContext();
        }
        public void Add(Realtor entity) {
            context.Realtors.Add(entity);
            context.SaveChanges();
        }

        public Realtor Get(int id) => context.Realtors.Where(entity => entity.ID == id).SingleOrDefault();

        public IEnumerable<Realtor> GetAll() => context.Realtors;

        public void Remove(int id) {
            context.Realtors.Remove(Get(id));
            context.SaveChanges();
        }

        public void Update(int id, Realtor entity) {
            if (id != entity.ID) return;
            Realtor original = Get(id);
            original.Name = entity.Name;
            original.Balance = entity.Balance;
            context.SaveChanges();
        }
    }
}
