﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tkachenko.Roman.SellHome.Model.Entity;

namespace Tkachenko.Roman.SellHome.API.Services {
    public interface IRealtorService {
        public Realtor Get(int id);
        public IEnumerable<Realtor> GetAll();
        public void Add(Realtor entity);
        public void Remove(int id);
        public void Update(int id, Realtor entity);
    }
}
