﻿using System;
using System.Collections.Generic;
using Tkachenko.Roman.SellHome.Model.Entity;

namespace Tkachenko.Roman.SellHome.API.Services {
    public interface IOperationService {
        public Operation Get(int id);
        public IEnumerable<Operation> GetAll();
        public void Add(Operation entity);
        public void Remove(int id);
        public void Update(int id, Operation entity);
    }
}
