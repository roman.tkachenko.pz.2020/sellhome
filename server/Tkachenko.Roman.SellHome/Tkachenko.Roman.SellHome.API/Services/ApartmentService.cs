﻿using System.Collections.Generic;
using System.Linq;
using Tkachenko.Roman.SellHome.Model;
using Tkachenko.Roman.SellHome.Model.Entity;

namespace Tkachenko.Roman.SellHome.API.Services {
    public class ApartmentService : IApartmentService {
        private SHDBContext context;
        public ApartmentService() {
            context = new SHDBContext();
        }
        public void Add(Apartment entity) {
            context.Apartments.Add(entity);
            context.SaveChanges();
        }

        public Apartment Get(int id) => context.Apartments.Where(entity => entity.ID == id).SingleOrDefault();

        public IEnumerable<Apartment> GetAll() => context.Apartments;

        public void Remove(int id) {
            context.Apartments.Remove(Get(id));
            context.SaveChanges();
        }

        public void Update(int id, Apartment entity) {
            if (id != entity.ID) return;
            Apartment original = Get(id);
            original.Area = entity.Area;
            original.Location = entity.Location;
            original.SchemaPath = entity.SchemaPath;
            original.Price = entity.Price;
            context.SaveChanges();
        }
    }
}
