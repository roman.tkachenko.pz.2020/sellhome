﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tkachenko.Roman.SellHome.Model;
using Tkachenko.Roman.SellHome.Model.Entity;

namespace Tkachenko.Roman.SellHome.API.Services {
    public class OperationService : IOperationService {
        private SHDBContext context;
        public OperationService() {
            context = new SHDBContext();
        }
        public void Add(Operation entity) {
            context.Operations.Add(entity);
            context.SaveChanges();
        }

        public Operation Get(int id) => context.Operations.Where(entity => entity.ID == id).SingleOrDefault();

        public IEnumerable<Operation> GetAll() => context.Operations;

        public void Remove(int id) {
            context.Operations.Remove(Get(id));
            context.SaveChanges();
        }

        public void Update(int id, Operation entity) {
            if (id != entity.ID) return;
            Operation original = Get(id);
            original.Apartment = entity.Apartment;
            original.Realtor = entity.Realtor;
            original.BoughtBy = entity.BoughtBy;
            original.Bonus = entity.Bonus;
            context.SaveChanges();
        }
    }
}
