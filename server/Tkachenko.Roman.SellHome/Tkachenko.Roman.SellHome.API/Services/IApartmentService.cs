﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tkachenko.Roman.SellHome.Model.Entity;

namespace Tkachenko.Roman.SellHome.API.Services {
    public interface IApartmentService {
        public Apartment Get(int id);
        public IEnumerable<Apartment> GetAll();
        public void Add(Apartment entity);
        public void Remove(int id);
        public void Update(int id, Apartment entity);
    }
}
