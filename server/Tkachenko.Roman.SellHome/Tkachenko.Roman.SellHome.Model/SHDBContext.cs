﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using Tkachenko.Roman.SellHome.Model.Entity;

namespace Tkachenko.Roman.SellHome.Model {
    public class SHDBContext : DbContext {
        public DbSet<Apartment> Apartments { get; set; }
        public DbSet<Realtor> Realtors { get; set; }
        public DbSet<Operation> Operations { get; set; }

        public SHDBContext() : base("SellHomeDB") {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SHDBContext, Migrations.Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) { }
    }
}
