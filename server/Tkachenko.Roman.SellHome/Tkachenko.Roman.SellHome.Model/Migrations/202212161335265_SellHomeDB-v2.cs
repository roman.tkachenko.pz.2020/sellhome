﻿namespace Tkachenko.Roman.SellHome.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SellHomeDBv2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Operations", "Apartment_ID", "dbo.Apartments");
            DropForeignKey("dbo.Operations", "Realtor_ID", "dbo.Realtors");
            DropIndex("dbo.Operations", new[] { "Apartment_ID" });
            DropIndex("dbo.Operations", new[] { "Realtor_ID" });
            AddColumn("dbo.Operations", "Apartment", c => c.Int(nullable: false));
            AddColumn("dbo.Operations", "Realtor", c => c.Int(nullable: false));
            DropColumn("dbo.Operations", "Apartment_ID");
            DropColumn("dbo.Operations", "Realtor_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Operations", "Realtor_ID", c => c.Int());
            AddColumn("dbo.Operations", "Apartment_ID", c => c.Int());
            DropColumn("dbo.Operations", "Realtor");
            DropColumn("dbo.Operations", "Apartment");
            CreateIndex("dbo.Operations", "Realtor_ID");
            CreateIndex("dbo.Operations", "Apartment_ID");
            AddForeignKey("dbo.Operations", "Realtor_ID", "dbo.Realtors", "ID");
            AddForeignKey("dbo.Operations", "Apartment_ID", "dbo.Apartments", "ID");
        }
    }
}
