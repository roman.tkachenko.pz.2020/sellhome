﻿namespace Tkachenko.Roman.SellHome.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SellHomeDBv1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Apartments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Area = c.Single(nullable: false),
                        Location = c.String(),
                        SchemaPath = c.String(),
                        Price = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Operations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Bonus = c.Single(nullable: false),
                        BoughtBy = c.String(),
                        Apartment_ID = c.Int(),
                        Realtor_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Apartments", t => t.Apartment_ID)
                .ForeignKey("dbo.Realtors", t => t.Realtor_ID)
                .Index(t => t.Apartment_ID)
                .Index(t => t.Realtor_ID);
            
            CreateTable(
                "dbo.Realtors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Balance = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Operations", "Realtor_ID", "dbo.Realtors");
            DropForeignKey("dbo.Operations", "Apartment_ID", "dbo.Apartments");
            DropIndex("dbo.Operations", new[] { "Realtor_ID" });
            DropIndex("dbo.Operations", new[] { "Apartment_ID" });
            DropTable("dbo.Realtors");
            DropTable("dbo.Operations");
            DropTable("dbo.Apartments");
        }
    }
}
