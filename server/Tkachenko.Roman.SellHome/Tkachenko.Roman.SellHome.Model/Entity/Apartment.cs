﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tkachenko.Roman.SellHome.Model.Entity {
    public class Apartment {
        public int ID { get; set; }
        public float Area { get; set; }
        public string Location { get; set; }
        public string SchemaPath { get; set; }
        public float Price { get; set; }
    }
}
