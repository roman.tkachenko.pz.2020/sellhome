﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tkachenko.Roman.SellHome.Model.Entity {
    public class Realtor {
        public int ID { get; set; }
        public string Name { get; set; }
        public float Balance { get; set; }
    }
}
