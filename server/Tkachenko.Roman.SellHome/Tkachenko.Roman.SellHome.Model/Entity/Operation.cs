﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tkachenko.Roman.SellHome.Model.Entity {
    public class Operation {
        public int ID { get; set; }
        public int Apartment { get; set; }
        public int Realtor { get; set; }
        public float Bonus { get; set; }
        public string BoughtBy { get; set; }
    }
}
