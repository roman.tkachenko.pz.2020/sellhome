export async function finishDeal(apartment, realtor, bonus, customer) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            apartment: +apartment,
            realtor: +realtor,
            bonus: +bonus,
            boughtBy: customer
        })
    };
    return await fetch('https://localhost:44342/api/operation/new', requestOptions);
}