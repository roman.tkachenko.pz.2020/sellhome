import { NavLink } from "react-router-dom";

function ApartmentCard(props) {
    const images = require.context('../../img', true);
    let loadImage = imageName => images("./" + imageName).default;

    // TODO: fix image loading

    return <div className="d-flex flex-row w-100 py-1">
        <div className="d-flex justify-content-center align-items-center w-50 border">
            <img 
                // src={props.data.schemaPath} 
                src={loadImage("test.png")}
                alt={"Apartment " + props.data.id}
                style={{
                    maxWidth: "100%",
                    minHeight: "300px",
                    maxHeight: "300px"
                }}/>
        </div>
        <div className="w-50">
            <div className="ms-2">
                <p className="m-0"><strong>Location:</strong> {props.data.location}</p>
                <p className="m-0"><strong>Area:</strong> {props.data.area} m<sup>2</sup></p>
                <p className="m-0"><strong>Price:</strong> ${props.data.price}</p>
            </div>
            <NavLink 
                end
                to={"/sell/" + props.data.id}
                className="btn btn-secondary mt-3 ms-2">
                Sell apartment
            </NavLink>
        </div>
    </div>;
}

export default ApartmentCard;