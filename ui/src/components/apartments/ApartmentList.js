import ApartmentCard from "./ApartmentCard";

function ApartmentList(props){
    let apartments = []; 
    for (let apartment in props.apartments) {
        apartments.push(<tr key={"ap-" + props.apartments[apartment].id}>
            <td><ApartmentCard data={props.apartments[apartment]} /></td>
        </tr>);
    }
    return <table className="table table-striped">
        <thead>
            <tr>
                <th className="text-center">Apartments</th>
            </tr>
        </thead>
        <tbody>{apartments}</tbody>
    </table>;
}
 
export default ApartmentList;