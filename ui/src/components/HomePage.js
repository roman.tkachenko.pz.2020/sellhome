import { Component } from "react";
import ApartmentList from "./apartments/ApartmentList";

class HomePage extends Component {
    state = { 
        apartments: []
    }; 

    componentDidMount() {
        fetch("https://localhost:44342/api/apartment", {
            method: "GET"
        })
        .then(res => res.json())
        .then((result) => {
            this.setState({
                apartments: result
            });
        });
    }

    render() { 
        return <>
            <div className="container">
                <ApartmentList apartments={this.state.apartments}/>
            </div>
        </>;
    }
}
 
export default HomePage;