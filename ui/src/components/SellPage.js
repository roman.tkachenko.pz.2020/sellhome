import { NavLink } from "react-router-dom";

import { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { finishDeal } from "../utils/requests";

function SellPage(props) {
    // apartment id
    let { id } = useParams();
    // all possible realtors
    
    const [realtors, setRealtors] = useState();
    const reRef = useRef();
    const cuRef = useRef();
    const boRef = useRef();

    useEffect(() => {   
        fetch("https://localhost:44342/api/realtor", {
            method: "GET"
        })
        .then(res => res.json())
        .then((result) => {
            setRealtors(result);
        });
    }, []);

    let realtorOptions = [];
    if(realtors) {
        for (let r of realtors) {
            realtorOptions.push(<option key={"re-" + r.id} value={r.id}>{r.name}</option>)
        }
    }
    return <>
        <h3 className="text-center">Sell Apartment</h3>
        <div className="row my-2">
            <div className="col-2"><strong>Apartment ID:</strong></div> 
            <div className="col">{id}</div>
        </div>
        <div className="row my-2">
            <div className="col-2"><strong>Bonus (%):</strong></div> 
            <div className="col">
                <input type="number" className="ps-1" defaultValue="0" ref={boRef}/>
            </div>
        </div>
        <div className="row my-2">
            <div className="col-2"><strong>Realtor:</strong></div>
            <div className="col">
                <select ref={reRef}>
                    {realtorOptions}
                </select>
            </div>
        </div>
        <div className="row my-2">
            <div className="col-2"><strong>Customer name:</strong></div>
            <div className="col">
                <input type="text" placeholder="Name..." ref={cuRef}/>
            </div>
        </div>
        <div className="row d-flex flex-column align-items-center w-100">
            <NavLink 
                end
                to={"/"}
                className="btn btn-secondary mt-3 ms-2"
                onClick={() => {
                    finishDeal(id, reRef.current.value, boRef.current.value, cuRef.current.value)
                }}>
                Finish deal
            </NavLink>
            <NavLink 
                end
                to={"/"}
                className="btn btn-link ms-2 w-50">
                Back to apartments
            </NavLink>
        </div>
    </>;
}

export default SellPage;