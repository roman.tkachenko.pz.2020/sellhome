import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import HomePage from "./components/HomePage";
import SellPage from "./components/SellPage";

function App() {
  return (
    <div className="App">
      <Router>
        <div className="container">
          <Routes>
              <Route exact path="/" element={<HomePage/>}/>
              <Route exact path="/sell/:id" element={<SellPage/>}/>
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
